package NODOS;

/* 
 * @author BLADIMIR
 */
public class Nodo<T> {

    private T dato;
    private Nodo<T> alSiguiente;//auto referencia

    public Nodo() {
    }

    public Nodo(T dato, Nodo<T> alSiguiente) {
        this.dato = dato;
        this.alSiguiente = alSiguiente;
    }

    public Nodo(T dato) {
        this.dato = dato;
        this.alSiguiente = null;
    }

    public T getDato() {
        return dato;
    }

    public void setDato(T dato) {
        this.dato = dato;
    }

    public Nodo<T> getAlSiguiente() {
        return alSiguiente;
    }

    public void setAlSiguiente(Nodo<T> alSiguiente) {
        this.alSiguiente = alSiguiente;
    }
}
