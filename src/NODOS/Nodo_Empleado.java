package NODOS;

import ENTIDADES.Empleado;

/* 
 * @author BLADIMIR
 */
public class Nodo_Empleado{

    private Empleado dato;
    
    private Nodo_Empleado alSiguiente;//auto referencia

    public Nodo_Empleado() {
    }

    public Nodo_Empleado(Empleado dato, Nodo_Empleado alSiguiente) {
        this.dato = dato; 
        this.alSiguiente = alSiguiente;
    }

    public Nodo_Empleado(Empleado dato) {
        this.dato = dato; 
        this.alSiguiente = null;
    }

    public Empleado getDato() {
        return dato;
    }

    public void setDato(Empleado dato) {
        this.dato = dato;
    }

    public Nodo_Empleado getAlSiguiente() {
        return alSiguiente;
    }

    public void setAlSiguiente(Nodo_Empleado alSiguiente) {
        this.alSiguiente = alSiguiente;
    }
}
