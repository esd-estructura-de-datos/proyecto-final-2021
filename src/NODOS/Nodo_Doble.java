package NODOS;

/**
 *
 * @author BLADIMIR
 */
public class Nodo_Doble<T> {

    T dato;
    Nodo_Doble anterior;
    Nodo_Doble siguiente;

    public Nodo_Doble() {
    }

    public Nodo_Doble(T dato) {
        this.dato = dato;
        anterior = null;
        siguiente = null;

    }

    public Nodo_Doble(T dato, Nodo_Doble anterior, Nodo_Doble siguiente) {
        this.dato = dato;
        this.anterior = anterior;
        this.siguiente = siguiente;
    }

    public T getDato() {
        return dato;
    }

    public void setDato(T dato) {
        this.dato = dato;
    }

    public Nodo_Doble getAnterior() {
        return anterior;
    }

    public void setAnterior(Nodo_Doble anterior) {
        this.anterior = anterior;
    }

    public Nodo_Doble getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo_Doble siguiente) {
        this.siguiente = siguiente;
    }

}
