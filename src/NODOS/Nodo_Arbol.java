package NODOS;

/**
 *
 * @author BLADIMIR
 */
public class Nodo_Arbol<T> {

    private T dato;
    private Nodo_Arbol izquierda;
    private Nodo_Arbol derecha;
    private int altura;

    public Nodo_Arbol() {
    }

    public Nodo_Arbol(T dato) {
        this.dato = dato;
        this.izquierda = null;
        this.derecha = null;
        this.altura=0;
    }

    public Nodo_Arbol(T dato, Nodo_Arbol izquierda, Nodo_Arbol derecha) {
        this.dato = dato;
        this.izquierda = izquierda;
        this.derecha = derecha;
        this.altura=0;
    }

    public T getDato() {
        return dato;
    }

    public void setDato(T dato) {
        this.dato = dato;
    }

    public Nodo_Arbol getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(Nodo_Arbol izquierda) {
        this.izquierda = izquierda;
    }

    public Nodo_Arbol getDerecha() {
        return derecha;
    }

    public void setDerecha(Nodo_Arbol derecha) {
        this.derecha = derecha;
    }

    @Override
    public String toString() {
        return dato + "";
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

}
