package DAO;

import CONEXION.Conexion;
import ENTIDADES.Categoria;
import VISTAS.Alerta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author
 */
public class DAO_Categoria {

    private Connection userConn;

    public DAO_Categoria() {
    }

    public DAO_Categoria(Connection userConn) {
        this.userConn = userConn;
    }

    public void insert(Categoria obj) throws SQLException {
        String SQL_INSERT = "insert into categoria(nombre)" + "VALUES('" + obj.getNombre() + "')";
        alterarRegistro(SQL_INSERT);
    }

    public void update(Categoria obj) throws SQLException {

        String SQL_UPDATE = "update categoria set nombre= '" + obj.getNombre() + " where idCategoria=" + obj.getIdCategoria() + "";
        alterarRegistro(SQL_UPDATE);
    }

    public void delete(Categoria obj) throws SQLException {
        String SQL_DELETE = " delete from categoria where idCategoria = '" + obj.getIdCategoria() + "'";
        alterarRegistro(SQL_DELETE);
    }

    public ArrayList<Categoria> selectALL() throws SQLException {
        String SQL_SELECT = " select * from Categoria";
        return select(SQL_SELECT);
    }

    public ArrayList<Categoria> selectId(int id) throws SQLException {
        String SQL_SELECT = " select * from Categoria where idCategoria=" + id;
        return select(SQL_SELECT);
    }

    public ArrayList<Categoria> buscar(String dato) throws SQLException {
        String SQL_SELECT = " select * from Categoria where nombre like'%" + dato + "'";
        return select(SQL_SELECT);
    }

    private void alterarRegistro(String sql) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            if (userConn != null) {
                conn = this.userConn;
            } else {
                conn = Conexion.getconexion();
            }
            ps = conn.prepareStatement(sql);
            ps.execute();

        } catch (Exception e) {
            e.printStackTrace();
            Alerta aler = new Alerta("Error proveedor con productos registrados", "/img/error.png");
            aler.show();
        } finally {
            ps.close();
            if (userConn == null) {
                Conexion.closeConexion(conn);
            }
        }
    }

    private ArrayList<Categoria> select(String sql) throws SQLException {
        ArrayList<Categoria> lista = new ArrayList<Categoria>();

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Categoria obj;
        try {
            if (userConn != null) {
                conn = this.userConn;
            } else {
                conn = Conexion.getconexion();
            }
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new Categoria();
                obj.setIdCategoria(rs.getInt(1));
                obj.setNombre(rs.getString(2));
                lista.add(obj);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error");
        } finally {
            ps.close();
            rs.close();
            if (userConn == null) {
                Conexion.closeConexion(conn);
            }
        }
        return lista;
    }
}
