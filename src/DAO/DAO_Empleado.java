package DAO;

import CONEXION.Conexion;
import ENTIDADES.Empleado;
import LISTAS.Lista_Simple_Empleados;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAO_Empleado {

    private Connection userConn;
    
     private Lista_Simple_Empleados listaem ;
     
    public DAO_Empleado() {
       this.listaem = new Lista_Simple_Empleados();
    }

    public DAO_Empleado(Connection userConn) {
        this.userConn = userConn;
        this.listaem = new Lista_Simple_Empleados();
    }
////
////    public Lista_Simple_Empleados getListaem() {
////        return listaem;
////    }
////
////    public void setListaem(Lista_Simple_Empleados listaem) {
////        this.listaem = listaem;
////    }
    

    
    public void insert(Empleado obj) throws SQLException {
        String SQL_INSERT = "insert into Empleado(idCargo,salario,nombre,apellido,dui,nit,telefono) VALUES(" + obj.getIdCargo()
                + "," + obj.getSalario() + ",'" + obj.getNombre() + "','" + obj.getApellido() + "','"
                + obj.getDui() + "','" + obj.getNit() + "','" + obj.getTelefono()
                + "')";
        System.out.println("INSERTANDO EN LA BASE");
        alterarRegistro(SQL_INSERT);
        listaem.insertar(obj);// inserta el empleado en la lista simple
    }

    public void update(Empleado obj) throws SQLException {
        String SQL_UPDATE = "update Empleado set dui='" + obj.getDui() + "',nombre='" + obj.getNombre() + "',apellido='" + obj.getApellido()
                + "',Nit='" + obj.getNit() + "',telefono='" + obj.getTelefono() + "', idCargo=" + obj.getIdCargo() + ", salario=" + obj.getSalario() + " where idEmpleado=" + obj.getIdPersona() + "";
        alterarRegistro(SQL_UPDATE);
//        Empleado buscar =  new Empleado();
//        buscar=obj;
//        listaem.buscar(obj.getIdPersona()).setDato(buscar);

    }

    public void delete(Empleado obj) throws SQLException {
        String SQL_DELETE = " delete from Empleado where idEmpleado = '" + obj.getIdPersona() + "'";
        alterarRegistro(SQL_DELETE);
    }

    public Lista_Simple_Empleados selectALL() throws SQLException {///para mostrar datos 
        String SQL_SELECT = "SELECT\n"
                + "	empleado.idEmpleado, \n"
                + "	empleado.idCargo, \n"
                + "	empleado.salario, \n"
                + "	empleado.nombre, \n"
                + "	empleado.apellido, \n"
                + "	empleado.dui, \n"
                + "	empleado.nit, \n"
                + "	empleado.telefono,\n"
                + "	cargo.cargo\n"
                + "FROM\n"
                + "	empleado\n"
                + "	INNER JOIN\n"
                + "	cargo\n"
                + "	ON \n"
                + "		empleado.idCargo = cargo.idCargo";
        return select(SQL_SELECT);
    }

    public Lista_Simple_Empleados selectId(int id) throws SQLException {// busca al empelado por el id
        String SQL_SELECT = " select * from Empleado where idPersona=" + id;
        return select(SQL_SELECT);
    }

    public Lista_Simple_Empleados buscar(String dato) throws SQLException {//buscar sirve para buscar por nombre dui o apellido
        String SQL_SELECT = " select * from Empleado where dui like'%" + dato + "' or nombre like '%" + dato + "' or apellido like '%" + dato + "'";
        return select(SQL_SELECT);
    }

    private void alterarRegistro(String sql) throws SQLException {// modificar o agregar un registro de la base
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            if (userConn != null) {
                conn = this.userConn;
            } else {
                conn = Conexion.getconexion();
            }
            ps = conn.prepareStatement(sql);
            ps.execute();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error");
        } finally {
            ps.close();
            if (userConn == null) {
                Conexion.closeConexion(conn);
            }
        }
    }

    private Lista_Simple_Empleados select(String sql) throws SQLException {// selecciona  con la lista
        Lista_Simple_Empleados lista = new Lista_Simple_Empleados();

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Empleado obj;
        try {
            if (userConn != null) {
                conn = this.userConn;
            } else {
                conn = Conexion.getconexion();
            }
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new Empleado();
                obj.setIdPersona(rs.getInt(1));
                obj.setIdCargo(Integer.parseInt(rs.getString(2)));
                obj.setSalario(rs.getDouble(3));
                obj.setNombre(rs.getString(4));
                obj.setApellido(rs.getString(5));
                obj.setDui(rs.getString(6));

                obj.setNit(rs.getString(7));
                obj.setTelefono(rs.getString(8));
                obj.setCargo(rs.getString(9));
//              obj.setFechaNacimiento(rs.getDate(7));
            
                lista.insertar(obj);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error");
        } finally {
            ps.close();
            rs.close();
            if (userConn == null) {
                Conexion.closeConexion(conn);
            }
        }
        listaem=lista;
        return listaem;
    }
}
