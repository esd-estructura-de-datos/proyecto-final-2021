package DAO;

import CONEXION.Conexion;
import ENTIDADES.Producto;
import PILA_COLA.Cola;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAO_Producto {

    private Connection userConn;

    public DAO_Producto() {
    }

    public DAO_Producto(Connection userConn) {
        this.userConn = userConn;
    }

    public void insert(Producto obj) throws SQLException {
        String SQL_INSERT = "insert into producto(codigoProducto,nombre,cantidad,precio,precioVenta,idCategoria,idProveedor) VALUES('" + obj.getCodigoProducto() + "','" + obj.getNombre()
                + "'," + obj.getCantidad() + "," + obj.getPrecio() + "," + obj.getPrecioVenta() + ","
                + obj.getIdCategoria() + "," + obj.getIdProveedor() + ")";
        alterarRegistro(SQL_INSERT);
    }

    public void update(Producto obj) throws SQLException {
        String SQL_UPDATE = "update producto set codigoProducto='" + obj.getCodigoProducto() + "',nombre='" + obj.getNombre() + "',cantidad=" + obj.getCantidad()
                + ",precio=" + obj.getPrecio() + ",precioVenta=" + obj.getPrecioVenta() + ", idCategoria=" + obj.getIdCategoria() + ", idProveedor=" + obj.getIdProveedor() + " where idProducto=" + obj.getIdProducto() + "";
        alterarRegistro(SQL_UPDATE);

    }

    public void delete(Producto obj) throws SQLException {
        String SQL_DELETE = " delete from producto where idProducto = " + obj.getIdProducto() + "";
        alterarRegistro(SQL_DELETE);
    }

    public ArrayList<Producto> selectALL() throws SQLException {
        String SQL_SELECT = " select * from producto";
        return select(SQL_SELECT);
    }

    public ArrayList<Producto> select1ALL() throws SQLException {
        String SQL_SELECT = " select * from producto";
        return select(SQL_SELECT);
    }

    public ArrayList<Producto> selectId(int id) throws SQLException {
        String SQL_SELECT = " select * from producto where idProducto=" + id;
        return select(SQL_SELECT);
    }
    public Cola<Producto> selectIdCola(int id) throws SQLException {
        String SQL_SELECT = " select * from producto where idProducto=" + id;
        return selectCola(SQL_SELECT);
    }

    public ArrayList<Producto> select(String nombre, int cantidad, double precioVenta) throws SQLException {
        String SQL_SELECT = " select * from producto where nombre=" + nombre + " where cantidad=" + cantidad + " where precioVenta=" + precioVenta;
        return select(SQL_SELECT);
    }

    public ArrayList<Producto> buscar(String dato) throws SQLException {
        String SQL_SELECT = " select * from producto where codigoProducto  like'%" + dato + "'";
        return select(SQL_SELECT);
    }

    private void alterarRegistro(String sql) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            if (userConn != null) {
                conn = this.userConn;
            } else {
                conn = Conexion.getconexion();
            }
            ps = conn.prepareStatement(sql);
            ps.execute();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error");
        } finally {
            ps.close();
            if (userConn == null) {
                Conexion.closeConexion(conn);
            }
        }
    }

    private ArrayList<Producto> select(String sql) throws SQLException {
        ArrayList<Producto> lista = new ArrayList();

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Producto obj;
        try {
            if (userConn != null) {
                conn = this.userConn;
            } else {
                conn = Conexion.getconexion();
            }
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new Producto();
                obj.setIdProducto(rs.getInt(1));
                obj.setCodigoProducto(rs.getString(2));
                obj.setNombre(rs.getString(3));
                obj.setCantidad(rs.getInt(4));
                obj.setPrecio(rs.getDouble(5));
                obj.setPrecioVenta(rs.getDouble(6));
                obj.setIdCategoria(rs.getInt(7));
                obj.setIdProveedor(rs.getInt(8));

                lista.add(obj);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error");
        } finally {
            ps.close();
            rs.close();
            if (userConn == null) {
                Conexion.closeConexion(conn);
            }
        }
        return lista;
    }

    private ArrayList<Producto> select1(String sql) throws SQLException {
        ArrayList<Producto> lista = new ArrayList();

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Producto obj;
        try {
            if (userConn != null) {
                conn = this.userConn;
            } else {
                conn = Conexion.getconexion();
            }
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new Producto();
                obj.setIdProducto(rs.getInt(1));
                obj.setCodigoProducto(rs.getString(2));
                obj.setNombre(rs.getString(3));
                obj.setCantidad(rs.getInt(4));

                obj.setPrecioVenta(rs.getDouble(5));

//              obj.setFechaNacimiento(rs.getDate(7));
                lista.add(obj);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error");
        } finally {
            ps.close();
            rs.close();
            if (userConn == null) {
                Conexion.closeConexion(conn);
            }
        }
        return lista;
    }
    
    private Cola<Producto> selectCola(String sql) throws SQLException {
        Cola<Producto> lista = new Cola();

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Producto obj;
        try {
            if (userConn != null) {
                conn = this.userConn;
            } else {
                conn = Conexion.getconexion();
            }
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new Producto();
                obj.setIdProducto(rs.getInt(1));
                obj.setCodigoProducto(rs.getString(2));
                obj.setNombre(rs.getString(3));
                obj.setCantidad(rs.getInt(4));
                obj.setPrecio(rs.getDouble(5));
                obj.setPrecioVenta(rs.getDouble(6));
                obj.setIdCategoria(rs.getInt(7));
                obj.setIdProveedor(rs.getInt(8));

                lista.offer(obj);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error");
        } finally {
            ps.close();
            rs.close();
            if (userConn == null) {
                Conexion.closeConexion(conn);
            }
        }
        return lista;
    }

}
