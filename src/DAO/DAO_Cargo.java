package DAO;

import ARBOL_BINARIO.Arbol_BB;
import CONEXION.Conexion;
import ENTIDADES.Cargo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BLADIMIR
 */
public class DAO_Cargo {

    private Connection userConn;

    public DAO_Cargo() {
    }

    public boolean insert(Cargo obj) throws SQLException {
        String SQL_INSERT = "insert into cargo(idCargo,cargo) VALUES(?,?)";
        return alterarRegistro(SQL_INSERT, obj);
    }

    public void update(Cargo obj) throws SQLException {
        String SQL_UPDATE = "update cargo set cargo= '" + obj.getCargo() + "' where idCargo='" + obj.getIdCargo() + "'";
        alterarRegistro(SQL_UPDATE, obj);
    }

    public Arbol_BB<Cargo> selectALL() throws SQLException {
        String SQL_SELECT = " select * from cargo";
        return select(SQL_SELECT);
    }
    
        public Arbol_BB<Cargo> selectAllTo(String atributo, String condicion) throws SQLException {
        String SQL_SELECT = "select * from cargo where " + atributo + "='" + condicion + "'";
        return select(SQL_SELECT);
    }


    public ArrayList<Cargo> buscar(String dato) throws SQLException {
        String SQL_SELECT = " select * from cargo where cargo like'%" + dato + "'";
        return select(SQL_SELECT).IND();//USO
    }

    private boolean alterarRegistro(String sql, Cargo obj) throws SQLException {
        PreparedStatement ps = null;
        try {
            userConn = Conexion.getconexion();
            ps = userConn.prepareStatement(sql);
            ps.setInt(1, obj.getIdCargo());
            ps.setString(2, obj.getCargo());
            ps.execute();
            return true;
        } catch (Exception e) {
            System.out.println("Error en DAO Cargo");
            e.printStackTrace();
        } finally {
            ps.close();
            Conexion.closeConexion(userConn);
        }
        return false;
    }

    private Arbol_BB<Cargo> select(String sql) throws SQLException {
        Arbol_BB<Cargo> lista = new Arbol_BB();

        PreparedStatement ps = null;
        ResultSet rs = null;
        Cargo obj = null;
        try {
            userConn = Conexion.getconexion();
            ps = userConn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new Cargo();
                obj.setIdCargo(rs.getInt("idCargo"));
                obj.setCargo(rs.getString("cargo"));
                lista.insertar(obj);
            }
        } catch (Exception e) {
            System.out.println("Error en DAO CARGO SELECT");
            e.printStackTrace();
        } finally {
            ps.close();
            Conexion.closeConexion(userConn);
        }
        return lista;
    }

    public boolean delete(int obj) throws SQLException {
        String SQL_DELETE = " delete from cargo where idCargo = '" + obj + "'";
        PreparedStatement ps = null;
        try {
            userConn = Conexion.getconexion();
            ps = userConn.prepareStatement(SQL_DELETE);
            ps.execute();
            return true;
        } catch (Exception e) {
            System.out.println("Error en DAO");
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(DAO_Proveedores.class.getName()).log(Level.SEVERE, null, ex);
            }
            Conexion.closeConexion(userConn);
        }
        return false;
    }

//    public ArrayList<Cargo> selectId(int id) throws SQLException {
//        String SQL_SELECT = " select * from cargo where idCargo=" + id;
//        return select(SQL_SELECT);
//    }
}
