package DAO;

import ENTIDADES.Proveedor;
import CONEXION.Conexion;
import LISTAS.Lista_Doble_Circular;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BLADIMIR
 */
public class DAO_Proveedores {

    private Connection userConn;

    public DAO_Proveedores() {
    }

    public boolean insert(Proveedor obj) throws SQLException {
        String SQL_INSERT = "insert into proveedor(nombre,telefono,direccion) VALUES(?,?,?)";
        return alterarRegistro(SQL_INSERT, obj);
    }

    public void update(Proveedor obj) throws SQLException {
        String SQL_UPDATE = "update Proveedor set nombre= '" + obj.getNombre() + "' where idProveedor=" + obj.getIdProveedor() + "'";
        alterarRegistro(SQL_UPDATE, obj);
    }

    public Lista_Doble_Circular<Proveedor> selectALL() throws SQLException {
        String SQL_SELECT = " select * from Proveedor";
        return select(SQL_SELECT);
    }

    public Lista_Doble_Circular<Proveedor> selectAllTo(String atributo, String condicion) throws SQLException {
        String SQL_SELECT = "select * from proveedor where " + atributo + "='" + condicion + "'";
        return select(SQL_SELECT);
    }

    public ArrayList<Proveedor> buscar(String dato) throws SQLException {
        String SQL_SELECT = " select * from Proveedor where nombre like'%" + dato + "'";
        return select(SQL_SELECT).toArrayAsc();
    }

    public boolean delete(int obj) throws SQLException {
        String SQL_DELETE = " delete from Proveedor where idProveedor = '" + obj + "'";
        PreparedStatement ps = null;
        try {
            userConn = Conexion.getconexion();
            ps = userConn.prepareStatement(SQL_DELETE);
            ps.execute();
            return true;
        } catch (Exception e) {
            System.out.println("Error en sql");
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(DAO_Proveedores.class.getName()).log(Level.SEVERE, null, ex);
            }
            Conexion.closeConexion(userConn);
        }
        return false;
    }

    private Lista_Doble_Circular<Proveedor> select(String sql) throws SQLException {
        Lista_Doble_Circular<Proveedor> lista = new Lista_Doble_Circular();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        Proveedor obj = null;
        try {
            userConn = Conexion.getconexion();
            ps = userConn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new Proveedor();
                obj.setIdProveedor(rs.getInt("idProveedor"));
                obj.setNombre(rs.getString("nombre"));
                obj.setTelefono(rs.getString("telefono"));
                obj.setDireccion(rs.getString("direccion"));
                lista.insertar(obj);
            }
        } catch (Exception e) {
            System.out.println("Error en sql");
            e.printStackTrace();
        } finally {
            ps.close();
            Conexion.closeConexion(userConn);
        }
        return lista;
    }

    private boolean alterarRegistro(String sql, Proveedor obj) throws SQLException {
        PreparedStatement ps = null;
        try {
            userConn = Conexion.getconexion();
            ps = userConn.prepareStatement(sql);
            ps.setString(1, obj.getNombre());
            ps.setString(2, obj.getTelefono());
            ps.setString(3, obj.getDireccion());
            ps.execute();
            return true;
        } catch (Exception e) {
            System.out.println("Error en sql");
            e.printStackTrace();
        } finally {
            ps.close();
            Conexion.closeConexion(userConn);
        }
        return false;
    }

//    public ArrayList<Proveedor> selectId(int id) throws SQLException {
//        String SQL_SELECT = " select * from Proveedor where idProveedor=" + id;
//        return select(SQL_SELECT);
//    }
//
}
