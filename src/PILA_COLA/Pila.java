package PILA_COLA;

import NODOS.Nodo;
import java.util.ArrayList;

/**
 *
 * @author BLADIMIR
 */
public class Pila<T> {//parametrisar primero 

    private Nodo<T> stack;

    /*
    para que funcione esto de forma generica, es un nodo
    generico llamado pila para que funciones con otros tipos de datos
     */
    public Pila() {//constructor
        this.stack = null;//pila vacia
    }

    public void push(T d) {//traduccion empujar
        Nodo firstNodo = new Nodo(d);
        firstNodo.setAlSiguiente(stack);
        stack = firstNodo;
        /**
         * creamos un objeto nodo al cual le pasaremos un dato al constructor
         * que resive solo un dato gracias al constructor que mantiene el dato
         * se lo podemos pasar a firstNodo, firstNodo es un objeto que esta aqui
         * pero que puede acceder al los metodos de su clase.
         *
         * firstNodo envia su dato atraves del metodo setAlSiguiente (crado por
         * nosotros) a stack dicho stack se ira quedando en el fondo de la pila
         * cada ves que obtengamos un dato. Aclarar que el atributo AlSiguiente
         * es un espacio de memoria de tipo Nodo, es la segunda parte del Nodo,
         * por lo cual cuando enviamos se creaa otra stack
         *
         * en principio stack estara vacio por lo cual cuando llamemos a push en
         * otra clase este traera un solo dato, en ese momento stack ya no
         * estara vacio.
         */
    }

    public void push_2(T s) {
        Nodo nuevoNodo = new Nodo(s, stack);
        //firstNodo.setAlSiguiente(stack);
        stack = nuevoNodo;
    }

    public T pop() {
        //traduccion popular
        T cima = (T) "Pila Vacia";
        if (!isEmpty()) {
            cima = stack.getDato();//se salva el ultimo en entrar y despues lo limpia java
            stack = stack.getAlSiguiente();//ahora gracias a stack obtenemos el que estaba anterior a la cima y lo pasamos a la cima
        }
        return cima;//regresa el ultimo ingresado y se limpia
    }

    public boolean isEmpty() {
        return stack == null;
    }

    public T peek() {
        //traduccion Ojeada

        if (!isEmpty()) {
            return stack.getDato();
        }
        return (T) "Pila Vacia";
    }

    public ArrayList<T> toArray() {
        // to array 
        // traduccion A la matriz
        // verbo: formar, adornar, organizar,
        Nodo aux = stack;//nodo auxiliar que se le pasaran datos cada vuelta del while y que se iran almacenando en  un ArrayList

        ArrayList<T> listaMatriz = new ArrayList();

        while (aux != null) {
            listaMatriz.add((T) aux.getDato());
            aux = aux.getAlSiguiente(); //se pasa del dato actual al siguiente en cada vuelta
        }
        return listaMatriz;
    }
}
