package PILA_COLA;

import NODOS.Nodo;
import java.util.ArrayList;

/**
 *
 * @author BLADIMIR
 */
public class Cola<T> {

    private Nodo<T> inicio;
    private Nodo<T> fin;

    public Cola() {//Constructor
        inicio = null;
        fin = null;
    }

    public boolean Esta_vacio() {
        return inicio == null;
    }

    public void offer(T d) {
        //traduccion de offer = ofrecer
        Nodo newNodo = new Nodo(d);
        if (Esta_vacio()) {
            inicio = newNodo;
        } else {
            fin.setAlSiguiente(newNodo);
        }
        fin = newNodo;
    }

    public T poll() {
        //traduccion sondear y quitar
        if (Esta_vacio()) {
            return (T) "Cola vacia";
        }
        T auxiliar = (T) inicio.getDato();

        if (inicio.getAlSiguiente() == null) {
            inicio = fin = null;
        } else {
            inicio = inicio.getAlSiguiente();
        }
        return auxiliar;
    }

    public ArrayList<T> toArray() {
        // to array 
        // traduccion A la matriz
        // verbo: formar, adornar, organizar,
        Nodo aux = inicio;//nodo auxiliar que se le pasaran datos cada vuelta del while y que se iran almacenando en  un ArrayList

        ArrayList listaMatriz = new ArrayList();

        while (aux != null) {
            listaMatriz.add(aux.getDato());
            aux = aux.getAlSiguiente(); //se pasa del dato actual al siguiente en cada vuelta
        }
        return listaMatriz;
    }

}
