package LISTAS;

import NODOS.Nodo_Doble;
import java.util.ArrayList;

/**
 *
 * @author BLADIMIR
 */
public class Lista_Doble_Circular<T> {

    Nodo_Doble lista;

    public Lista_Doble_Circular() {
        lista = null;
    }

    public boolean isEmpty() {
        return lista == null;
    }

    /*---- NUEVOS METODOS ----------------------------------------------------*/
    public <T extends Comparable> void insertarInicio(T dato) {
        Nodo_Doble nuevo = new Nodo_Doble(dato);
        if (isEmpty()) {
            lista = nuevo;
            nuevo.setSiguiente(lista);// new code
        }
    }

    public <T extends Comparable> void insertarAntes(T dato, T indicado) {

        Nodo_Doble nuevo = new Nodo_Doble(dato);
        Nodo_Doble anterior = ubicar(indicado);

        nuevo.setSiguiente(anterior.getSiguiente());//old code
        if (anterior.getSiguiente() != null) {
            anterior.getSiguiente().setAnterior(nuevo);
        }
    }

    public <T extends Comparable> void insertarAlFinal(T dato) {

        Nodo_Doble nuevo = new Nodo_Doble(dato);
        Nodo_Doble anterior = ubicar(dato);

        nuevo.setSiguiente(anterior.getSiguiente());//old code
        anterior.setSiguiente(nuevo);//old code//
        nuevo.setAnterior(anterior);
    }

    /*--- FIN DE NUEVOS METODOS ----------------------------------------------*/

    public ArrayList<T> toArrayAsc() {//toArray
        Nodo_Doble aux = lista;
        ArrayList<T> array = new ArrayList();

        if (!isEmpty()) {// new code
            do {// new code
                array.add((T) aux.getDato());
                aux = aux.getSiguiente();
            } while (aux != lista);// new code
        }
        return array;
    }

    public ArrayList<T> toArrayDes() {
        Nodo_Doble aux = lista;
        ArrayList<T> array = new ArrayList();

        do {// new code
            aux = aux.getSiguiente();
        } while (aux.getSiguiente() != lista);// new code

        do {// new code
            array.add((T) aux.getDato());
            aux = aux.getAnterior();
        } while (aux.getSiguiente() != lista);// new code

        return array;
    }

    public int size() {
        return toArrayAsc().size();
    }

    public <T extends Comparable> void insertar(T dato) {
        Nodo_Doble nuevo = new Nodo_Doble(dato);

        if (isEmpty()) {
            lista = nuevo;
            nuevo.setSiguiente(lista);// new code
        } else if (dato.compareTo(lista.getDato()) < 0) {

            Nodo_Doble ultimo = ultimo();// new code

            nuevo.setSiguiente(lista);
            lista.setAnterior(nuevo);
            lista = nuevo;

            ultimo.setSiguiente(lista);// new code

        } else {//entre dos nodos o al final
            Nodo_Doble anterior = ubicar(dato);
            nuevo.setSiguiente(anterior.getSiguiente());//old code
            if (anterior.getSiguiente() != null) {
                anterior.getSiguiente().setAnterior(nuevo);
            }
            anterior.setSiguiente(nuevo);//old code//
            nuevo.setAnterior(anterior);
        }
    }

    private <T extends Comparable> Nodo_Doble ubicar(T dato) {
        Nodo_Doble aux = lista;
        Nodo_Doble ant = lista;
        while (aux.getSiguiente() != lista && (dato.compareTo(aux.getDato()) > 0)) {// new code
            ant = aux;
            aux = aux.getSiguiente();
        }
        if (dato.compareTo(aux.getDato()) > 0) {
            ant = aux;
        }
        return ant;
    }

    public <T extends Comparable> Nodo_Doble buscar(T dato) {
        Nodo_Doble aux = lista;
        if (aux != null) {// new code
            do {// new code
                if (dato.compareTo(aux.getDato()) == 0) {
                    return aux;
                }
                aux = aux.getSiguiente();
            } while (aux != lista);// new code
        }
        return null;
    }

    public <T extends Comparable> void eliminar(T dato) {
        Nodo_Doble quitar = buscar(dato);
        if (quitar != null) {
            if (quitar == lista) {
                lista = quitar.getSiguiente();
                if (lista != lista) {//nuevo code // new code
                    lista.setAnterior(null);//nuevo code
                }
            } else {
                Nodo_Doble ant = ubicar(dato);
                ant.setSiguiente(quitar.getSiguiente());
                if (quitar.getSiguiente() != lista) {//nuevo code// new code
                    quitar.getSiguiente().setAnterior(ant);//nuevo code
                }
            }
            quitar = null;
        }
    }

    private Nodo_Doble ultimo() { // new code
        Nodo_Doble aux = lista;
        while ((aux.getSiguiente() != lista)) {
            aux = aux.getSiguiente();
        }
        return aux;
    }
}
