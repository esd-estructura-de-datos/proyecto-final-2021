package LISTAS;

import NODOS.Nodo;
import java.util.ArrayList;

/**
 *
 * @author BLADIMIR
 */
public class Lista_Simple<T> {

    private Nodo lista;

    public Lista_Simple() {
        lista = null;
    }

    public boolean isEmpty() {
        if (lista == null) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<T> toArray() {
        Nodo aux = lista;
        ArrayList<T> a = new ArrayList();
        while (aux != null) {
            a.add((T) aux.getDato());
            aux = aux.getAlSiguiente();
        }
        return a;
    }

    public int size() {
        return toArray().size();
    }

    public <T extends Comparable> void insertar(T dato) {
        Nodo nuevo = new Nodo(dato);

        if (isEmpty()) {
            lista = nuevo;
        } else if (dato.compareTo(lista.getDato()) < 0) {//agrega al siguiente "A + B"
            nuevo.setAlSiguiente(lista);
            lista = nuevo;
        } else {//sirve para agregar en anterior y al final
            Nodo ant = ubicar(dato);
            nuevo.setAlSiguiente(ant.getAlSiguiente());
            ant.setAlSiguiente(nuevo);
        }
    }

    private <T extends Comparable> Nodo ubicar(T dato) {
        Nodo ant = lista;
        Nodo aux = lista;

        while (aux.getAlSiguiente() != null && dato.compareTo(aux.getDato()) > 0) {
            ant = aux;
            aux = aux.getAlSiguiente();
        }
        if (dato.compareTo(aux.getDato()) > 0) {
            ant = aux;
        }
        return ant;
    }

    public <T extends Comparable> Nodo buscar(T dato) {//buscar a la hora de eliminar
        Nodo aux = lista;

        while (aux != null) {
            if (dato.compareTo(aux.getDato()) == 0) {
                return aux;
            }
            aux = aux.getAlSiguiente();
        }
        return null;
    }

    public <T extends Comparable> void eliminar(T dato) {
        Nodo quitar = buscar(dato);
        if (quitar != null) {
            if (quitar == lista) {
                lista = quitar.getAlSiguiente();
            } else {
                Nodo ant = ubicar(dato);
                ant.setAlSiguiente(quitar.getAlSiguiente());
            }
            quitar = null;//libera la memoria
        }
    }

}
