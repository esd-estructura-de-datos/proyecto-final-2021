package LISTAS;

import ENTIDADES.Empleado;
import NODOS.Nodo;
import NODOS.Nodo_Empleado;
import java.util.ArrayList;

/**
 *
 * @author BLADIMIR
 */
public class Lista_Simple_Empleados {

    private Nodo_Empleado lista ;

    public Lista_Simple_Empleados() {
        lista = null;
    }

    public boolean isEmpty() {
        if (lista == null) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<Empleado> toArray() {
        Nodo_Empleado aux = lista;
        ArrayList<Empleado> a = new ArrayList();
        while (aux != null) {
            a.add((Empleado) aux.getDato());
            aux = aux.getAlSiguiente();
        }
        return a;
    }

    public int size() {
        return toArray().size();
    }

    public void insertar(Empleado dato) {
        Nodo_Empleado nuevo = new Nodo_Empleado(dato);
            
        if (isEmpty()) {
            lista = nuevo;             
        } else if (dato.getIdPersona() < lista.getDato().getEmpleado().getIdPersona()) {//agrega al siguiente "A + B"
            nuevo.setAlSiguiente(lista);
            lista = nuevo;
        } else {//sirve para agregar en anterior y al final
            Nodo_Empleado ant = ubicar(dato.getIdPersona());
            nuevo.setAlSiguiente(ant.getAlSiguiente());
            ant.setAlSiguiente(nuevo);
        }
    }

    private Nodo_Empleado ubicar(int dato) {
        Nodo_Empleado ant = lista;
        Nodo_Empleado aux = lista;

        while (aux.getAlSiguiente() != null && dato > aux.getDato().getEmpleado().getIdPersona()) {
            ant = aux;
            aux = aux.getAlSiguiente();
        }
        if (dato > aux.getDato().getEmpleado().getIdPersona()) {
            ant = aux;
        }
        return ant;
    }

    public Nodo_Empleado buscar(int dato) {//buscar a la hora de eliminar
        Nodo_Empleado aux = lista;

        System.out.println("LO QUE MUESTRA DATO: " + dato);
        while (aux != null) {
            if (dato == aux.getDato().getIdPersona()) {
                System.out.println("DATO ENCONTRADO: ");
                return aux;
            }
             System.out.println("POSICION ID"+ aux.getDato().getIdPersona());
            aux = aux.getAlSiguiente();
        }
        return null;
    }

//    public Nodo_Empleado  buscar(String dato){
//        Nodo_Empleado  aux = lista;
//        while(aux!= null){
//            if(dato.compareToIgnoreCase(aux.getDato().getDui())==0){
//            return aux;
//            }
//            aux=aux.getAlSiguiente();
//        }
//        return null;
//    }
//    
//    public Nodo_Empleado ubicar(String dato){
//       Nodo_Empleado aux = lista;
//        Nodo_Empleado ant = lista;
//        while((aux.getAlSiguiente()!=null)&&(dato.compareToIgnoreCase(aux.getDato().getDui())>0)){
//           ant = aux;
//           aux = aux.getAlSiguiente();
//        }
//        
//        if(dato.compareToIgnoreCase(aux.getDato().getDui())>0) ant = aux;
//        
//        return ant;
//    }
    
   
    
    public void eliminar(int dato) {
        Nodo_Empleado quitar = buscar(dato);
        if (quitar != null) {
            if (quitar == lista) {
                lista = quitar.getAlSiguiente();
            } else {
                Nodo_Empleado ant = ubicar(dato);
                ant.setAlSiguiente(quitar.getAlSiguiente());
            }
            quitar = null;//libera la memoria
        }
    }

    
    
    
    
}
