package CONEXION;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author
 */
public class Conexion {

    private static final String US = "root";
    private static final String PS = "";
    private static final String URL = "jdbc:mysql://localhost:3306/ferreteria";

    public static synchronized Connection getconexion() {

        Connection cn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection(URL, US, PS);
        } catch (Exception e) {

            System.out.println("Error");
            e.printStackTrace();
        }
        return cn;

    }

    public static void closeConexion(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        }
    }
}
