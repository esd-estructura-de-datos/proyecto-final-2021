package ENTIDADES;

public class Usuario {

    private int idUsuario;
    private String Usuario;
    private String clave;

    public Usuario() {
    }

    public Usuario(int idUsuario, String Usuario, String clave) {
        this.idUsuario = idUsuario;
        this.Usuario = Usuario;
        this.clave = clave;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

}
