package ENTIDADES;

/**
 *
 * @author
 */
public class Empleado extends Persona {
    private int idCargo;
    private String cargo;
    private String dui;
    private String nit;
    private String telefono;
    private double salario;
    private Persona empleado = new Persona();


    
    public Empleado() {
        this.empleado = new Persona();
    }
    
    public Empleado(int idCargo, String cargo, String dui, String nit, String telefono, double salario, int idPersona, String nombre, String apellido) {
//        super(idPersona, nombre, apellido);
       
        this.empleado.setIdPersona(idPersona);
         this.empleado.setNombre(nombre);
          this.empleado.setApellido(apellido);
        this.idCargo = idCargo;
        this.cargo = cargo;
        this.dui = dui;
        this.nit = nit;
        this.telefono = telefono;
        this.salario = salario;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Empleado(int idCargo, String dui, String nit, String telefono, double salario, String nombre, String apellido) {
        super(nombre, apellido);
        this.idCargo = idCargo;
        this.dui = dui;
        this.nit = nit;
        this.telefono = telefono;
        this.salario = salario;
    }

    

    public Empleado(String dui) {
        this.dui = dui;
    }

    public Empleado(int idCargo) {
        this.idCargo = idCargo;
    }

    public Empleado(int idCargo, double salario) {
        this.idCargo = idCargo;
        this.salario = salario;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public double getSalario() {
        return salario;
    }

    public int getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    public Persona getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Persona empleado) {
        this.empleado = empleado;
    }
    
    

}
