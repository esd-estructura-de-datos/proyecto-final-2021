package ENTIDADES;

import java.util.Date;

/**
 *
 * @author
 */
public class Comprobante_Factura {

    private int idFactura;
    private Date fecha;
    private int factura;
    private String tipo;
    private Producto producto;
    private int cantidad;

    private double total;
    private String cliente;

//    public comprobanteFactura(int i, Date date, int parseInt, int c, String text, double parseDouble) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    public String getCliente() {
        return cliente;
    }

    public Comprobante_Factura() {
    }

    public Comprobante_Factura(int idFactura, Date fecha, int factura, String tipo, Producto producto, int cantidad, double total, String cliente) {
        this.idFactura = idFactura;
        this.fecha = fecha;
        this.factura = factura;
        this.tipo = tipo;
        this.producto = producto;
        this.cantidad = cantidad;
        this.total = total;
        this.cliente = cliente;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getFactura() {
        return factura;
    }

    public void setFactura(int factura) {
        this.factura = factura;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

}
