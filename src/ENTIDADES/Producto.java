package ENTIDADES;

/**
 *
 * @author
 */
public class Producto {

    private int idProducto;
    private String codigoProducto;
    private String nombre;
    private int cantidad;
    private double precio;
    private double precioVenta;
    private int idProveedor;
    private String proveeedor;
    private int idCategoria;
    private String categoria;

    public Producto() {
    }

    public Producto(int idProducto, String codigoProducto, String nombre, int cantidad, double precio, double precioVenta, int idProveedor, String proveeedor, int idCategoria, String categoria) {
        this.idProducto = idProducto;
        this.codigoProducto = codigoProducto;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = precio;
        this.precioVenta = precioVenta;
        this.idProveedor = idProveedor;
        this.proveeedor = proveeedor;
        this.idCategoria = idCategoria;
        this.categoria = categoria;
    }

    public Producto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public Producto(String nombre, int cantidad, double precioVenta) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precioVenta = precioVenta;
    }

    public Producto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getProveeedor() {
        return proveeedor;
    }

    public void setProveeedor(String proveeedor) {
        this.proveeedor = proveeedor;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

}
