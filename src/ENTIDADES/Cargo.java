package ENTIDADES;

import java.util.ArrayList;

/**
 *
 * @author BLADIMIR
 */
public class Cargo implements Comparable<Cargo> {

    private int idCargo;
    private String cargo;
    private ArrayList<Empleado> empleado;

    //---------------------------------------------------
    public Cargo() {
    }

    public Cargo(int idCargo, String cargo) {
        this.idCargo = idCargo;
        this.cargo = cargo;
    }

    public Cargo(int idCargo) {
        this.idCargo = idCargo;
    }

    public Cargo(String cargo) {
        this.cargo = cargo;
    }
    //---------------------------------------------------

    public ArrayList<Empleado> getEmpleado() {
        return empleado;
    }

    public void setEmpleado(ArrayList<Empleado> empleado) {
        this.empleado = empleado;
    }

    public int getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return cargo;
    }

    @Override
    public int compareTo(Cargo t) {
        Cargo actual = this;
        return (actual.getCargo().compareToIgnoreCase(t.getCargo()));
    }

}
