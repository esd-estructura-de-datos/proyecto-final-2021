package ARBOL_BINARIO;

import NODOS.Nodo_Arbol;
import java.util.ArrayList;

/**
 *
 * @author BLADIMIR
 */
public class Arbol_Padre<T> {

    private Nodo_Arbol raiz;

    public Arbol_Padre() {
        raiz = null;
    }

    public boolean isEmpty() {
        return raiz == null;
    }

    protected ArrayList preOrdenNID(Nodo_Arbol raiz, ArrayList a) {//1 visita raiz
        if (raiz != null) {
            a.add(raiz.getDato()); //poner el dato en la raiz
            preOrdenNID(raiz.getIzquierda(), a);//rama izquierda
            preOrdenNID(raiz.getDerecha(), a);//rama derecha
        }
        return a;
    }

    protected ArrayList inOrdenIND(Nodo_Arbol raiz, ArrayList a) {
        if (raiz != null) {
            inOrdenIND(raiz.getIzquierda(), a);
            a.add(raiz.getDato());
            inOrdenIND(raiz.getDerecha(), a);
        }
        return a;
    }

    protected ArrayList postOrdenIDN(Nodo_Arbol raiz, ArrayList a) {
        if (raiz != null) {
            postOrdenIDN(raiz.getIzquierda(), a);
            postOrdenIDN(raiz.getDerecha(), a);
            a.add(raiz.getDato());
        }
        return a;
    }

    public <T extends Comparable> Nodo_Arbol buscar(T dato) {
        return buscar(dato, raiz);
    }

    private <T extends Comparable> Nodo_Arbol buscar(T dato, Nodo_Arbol raiz) {
        if (raiz == null) {
            return null;
        } else if (dato.compareTo(raiz.getDato()) == 0) {
            return raiz;
        } else if (dato.compareTo(raiz.getDato()) < 0) {
            return buscar(dato, raiz.getIzquierda());
        } else {
            return buscar(dato, raiz.getDerecha());
        }
    }

    public Nodo_Arbol getRaiz() {
        return raiz;
    }

    public void setRaiz(Nodo_Arbol raiz) {
        this.raiz = raiz;
    }
//
//    public ArrayList getArray() {
//        return array;
//    }
//
//    public void setArray(ArrayList array) {
//        this.array = array;
//    }
//
////    public int compare(T dato, Nodo nodo) {
////        return ((Alumno) dato).getNombre().compareToIgnoreCase(((Alumno) nodo.getDato()).getNombre());
////    }
    
//    public int compare(int dato, Nodo nodo) {
//        if (dato < (int) nodo.getDato()) {
//            return -1;
//        } else if (dato > (int) nodo.getDato()) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }
}
