package ARBOL_BINARIO;

import NODOS.Nodo_Arbol;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author BLADIMIR
 */
public class Arbol_BB<T> extends Arbol_Padre {

    public Arbol_BB() {
        super();
    }

    public <T extends Comparable> void insertar(T dato) {
        super.setRaiz(insertar(dato, super.getRaiz()));
    }

    private <T extends Comparable> Nodo_Arbol insertar(T dato, Nodo_Arbol raiz) {
        if (raiz == null) {
            raiz = new Nodo_Arbol(dato);
        } else if (dato.compareTo(raiz.getDato()) < 0) {
            Nodo_Arbol izquierda = insertar(dato, raiz.getIzquierda());
            raiz.setIzquierda(izquierda);
        } else if (dato.compareTo(raiz.getDato()) > 0) {
            Nodo_Arbol derecha = insertar(dato, raiz.getDerecha());
            raiz.setDerecha(derecha);
        } else {
            JOptionPane.showMessageDialog(null, "Ya existe");
        }
        return raiz;
    }

    public ArrayList NID() {
        ArrayList x = new ArrayList();
        return super.preOrdenNID(super.getRaiz(), x);
    }

    public ArrayList IND() {
        ArrayList x = new ArrayList();
        return super.inOrdenIND(super.getRaiz(), x);
    }

    public ArrayList IDN() {
        ArrayList x = new ArrayList();
        return super.postOrdenIDN(super.getRaiz(), x);
    }

    private <T extends Comparable> void eliminar(T dato) {
        super.setRaiz(eliminar(dato, super.getRaiz()));
    }

    private <T extends Comparable> Nodo_Arbol eliminar(T dato, Nodo_Arbol r) {
        if (r == null) {
            JOptionPane.showMessageDialog(null, "No hay un nodo a eliminar");
        } else if (dato.compareTo(r.getDato()) < 0) {
            Nodo_Arbol izq;
            izq = eliminar(dato, r.getIzquierda());
            r.setIzquierda(izq);
        } else if (dato.compareTo(r.getDato()) > 0) {
            Nodo_Arbol dcho;
            dcho = eliminar(dato, r.getDerecha());
            r.setDerecha(dcho);
        } else {//---- NODO ENCONTRADO ---
            Nodo_Arbol q;
            q = r;
            if (q.getIzquierda() == null) {
                r = q.getDerecha();
            } else if (q.getDerecha() == null) {
                r = q.getIzquierda();
            } else {//---- CUANDO HAY DOS RAMAS O DOS HIJOS
                q = reemplazar(q);
            }
            q = null;
        }
        return r;
    }

    private Nodo_Arbol reemplazar(Nodo_Arbol actual) {
        Nodo_Arbol a, p;
        p = actual;
        a = actual.getIzquierda();

        while (a.getDerecha() != null) {
            p = a;
            a = a.getDerecha();
        }

        actual.setDato(a.getDato());

        if (p == actual) { //SI TIENE HIJOS A LA IZQUIERDA
            p.setIzquierda(a.getIzquierda());
        } else {
            p.setDerecha(a.getIzquierda());
        }
        return a;
    }

//    private NodoArbol buscar(NodoArbol nodo, String dato){
//        if (nodo == null) {
//            return null;
//        }else if (super.compare(dato, nodo) < 0) {
//            return buscar(nodo.getIzquierda(), dato);
//        }else if (super.compare(dato, nodo) > 0) {
//            return buscar(nodo.getDerecha(), dato);
//        }else{
//            return nodo;
//        }
//    }
//
//    private NodoArbol buscar(NodoArbol r, String d) {
//        if (r == null) {
//            return null;
//        } else if (d.compareToIgnoreCase(((Alumno) r.getDato()).getNombre()) == 0) {
//            return r;
//        } else if (d.compareToIgnoreCase(((Alumno) r.getDato()).getNombre()) < 0) {
//            return buscar(r.getIzquierda(), d);
//        } else {
//            return buscar(r.getDerecha(), d);
//        }
//    }
//
//    public NodoArbol buscar(String d) {
//        return buscar(super.getRaiz(), d);
//    }
//    public NodoArbol buscar(String dato){
//        if (super.getRaiz() == null) {
//            return null;
//        }else{
//            return buscar(super.getRaiz(), dato);
//        }
//    }
//    public void insertar(T dato) {
//        super.setRaiz(insertar(dato, super.getRaiz()));
//    }
//    private <T extends Comparable> Nodo insertar(T dato, Nodo raiz) {
//        if (raiz == null) {
//            raiz = new Nodo(dato);
//        } else if (super.compare(dato, raiz) < 0) {
//            Nodo izquierda = insertar(dato, raiz.getIzquierda());
//            raiz.setIzquierda(izquierda);
//        } else if (super.compare(dato, raiz) > 0) {
//            Nodo derecha = insertar(dato, raiz.getDerecha());
//            raiz.setDerecha(derecha);
//        } else {
//            JOptionPane.showMessageDialog(null, "Ya existe");
//        }
//        return raiz;
//    }
}
